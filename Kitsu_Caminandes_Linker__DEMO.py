import json
from pathlib import Path
from typing import List
from datetime import datetime

import bpy
from bpy.app.handlers import persistent
from bpy.utils import register_class, unregister_class
import opentimelineio as otio
import openreviewio as orio

# You can access all stax functions through this module
import stax

import stax.utils.utils_cache as utils_cache
import stax.utils.utils_config as utils_config
import stax.utils.utils_core as utils_core
from stax.utils.utils_linker import get_linker
import stax.utils.utils_openreviewio as utils_openreviewio

from stax.properties.properties_core import Track

# Install your own dependencies
try:
    import gazu

except ModuleNotFoundError:
    utils_core.install_dependency("gazu")
    import gazu


class KitsuCallbacks(stax.Callbacks):
    """Kitsu implementation as a Stax' callbacks script"""

    name: bpy.props.StringProperty(default="Kitsu 'Caminandes'")
    description = "Demo linker loading the Caminandes project from Félix David's server"

    kitsu_host = "https://kitsu.felixdavid.com/api"
    kitsu_login = ""
    kitsu_password = ""

    demo_directory: Path
    project_name: str
    tasks_list: List[str]

    # ======== Callbacks functions ============

    def authentication_exec(self, login, password):
        gazu.set_host(self.kitsu_host)
        token = gazu.log_in(login, password)
        gazu.client.set_tokens(token)

        self.kitsu_login = login
        self.kitsu_password = password

        return True if token else False

    def session_update_pre(self, timeline_path: str, reviews_folder: str = ""):
        # TODO Not optimized at all, update should be separated from timeline building

        # Download elements and build timeline
        timeline_path = Path(timeline_path)
        timeline_name = timeline_path.stem

        project = gazu.project.get_project_by_name(self.project_name)

        episode = gazu.shot.get_episode_by_name(project, timeline_name)

        sequences = gazu.shot.all_sequences_for_episode(episode)

        timeline = otio.schema.Timeline()

        # Set playback frame rate
        timeline.metadata["frame_rate"] = 25

        for seq in sequences:
            shots = gazu.shot.all_shots_for_sequence(seq)

            for shot in shots:
                tasks = gazu.task.all_tasks_for_shot(shot)

                fps = float(shot.get("data").get("fps"))
                start_time = otio.opentime.RationalTime(0, 1.0).rescaled_to(fps)
                end_time_exclusive = otio.opentime.from_frames(
                    int(shot.get("nb_frames")), fps
                )

                for task in tasks:  # TODO Arrange them
                    # If task not in selected tasks, continue
                    if task.get("task_type_name") not in self.tasks_list:
                        continue

                    # Download video files from kitsu server
                    comments = gazu.task.all_comments_for_task(task)
                    file_dir = self.demo_directory.joinpath(
                        task.get("task_type_name"), timeline_name
                    )
                    versions_list = []

                    if len(comments):
                        # Get versions and annotations from kitsu project
                        version_index = 1

                        version_name = ""
                        filename = ""  #  Kitsu: The first comment for a shot must contain a video preview
                        # This is because OpenReview associates a review to a media, then it must be a file.ext
                        version_media_name = ""

                        # Keep previous note for parenting if not new version
                        previous_note = None
                        for comment in reversed(
                            comments
                        ):  # Because comments are sorted from last to first in kitsu I need to crawl reversed
                            # Download annotations or versions
                            annotations_list = []
                            new_version = False  # Turn it to True if a new version
                            for i, preview in enumerate(comment.get("previews")):
                                if (
                                    preview.get("extension") == "mp4"
                                ):  # Versions (video files)
                                    version_name = (
                                        f"{seq.get('name')}_{shot.get('name')}_"
                                        f"{task.get('task_type_name')}_v{version_index}"
                                    )
                                    filename = (
                                        f"{version_name}.{preview.get('extension')}"
                                    )
                                    version_media_name = filename

                                    versions_list.append(file_dir.joinpath(filename))
                                    version_index += 1

                                    new_version = True

                                else:  # Annotations (images files)
                                    filename = f"{version_name}_{preview.get('revision')}-{i}.{preview.get('extension')}"
                                    annotations_list.append(file_dir.joinpath(filename))

                                file_path = file_dir.joinpath(filename)

                                if not file_path.is_file():
                                    if not file_path.parent.is_dir():
                                        file_path.parent.mkdir(parents=True)
                                    print("downloading: " + file_path.stem)
                                    gazu.files.download_preview_file(
                                        preview.get("id"), file_path
                                    )
                                else:
                                    print(
                                        f"{timeline_name}: {file_path.stem} already downloaded."
                                    )

                            #   Create associated notes
                            # TODO: a bit deprecated, waiting for gazu's developer informations

                            # Get annotated frames from comment text
                            annotation_text = comment.get("text").split(
                                "---"
                            )  # See self.reject_version()
                            contents_list = []
                            if len(annotation_text) > 1:
                                note_data = json.loads(
                                    annotation_text[-1].split("-")[-1]
                                )
                                annotations_frames = note_data.get("frames")

                                # Create contents for these annotations
                                if annotations_frames:
                                    previous_frame = -1
                                    for i, frame in enumerate(
                                        annotations_frames.values()
                                    ):
                                        relative_frame = frame - int(
                                            shot.get("data").get("frame_in")
                                        )

                                        contents_list.append(
                                            orio.Content.ImageAnnotation(
                                                path_to_image=file_dir.joinpath(
                                                    filename
                                                ),
                                                frame=relative_frame - previous_frame,
                                                duration=1,
                                            )
                                        )

                                        previous_frame = relative_frame

                            # Create media review
                            version_media_path = file_dir.joinpath(version_media_name)

                            # Timezone
                            tz = datetime.now().astimezone().tzinfo
                            # Date
                            task_date = str(
                                datetime.fromisoformat(task.get("updated_at")).replace(
                                    tzinfo=tz
                                )
                            )

                            # TODO This status part is not clean at all
                            if task.get("task_status_name", "") == "Retake":
                                status = orio.Status(
                                    state="rejected",
                                    author="stax",
                                    date=task_date,
                                )
                            elif task.get("task_status_name", "") == "Done":
                                status = orio.Status(
                                    state="approved",
                                    author="stax",
                                    date=task_date,
                                )
                            else:
                                status = orio.Status(
                                    state="waiting review",
                                    author="stax",
                                    date=task_date,
                                )

                            # Get review from Stax cache or create one if there isn't
                            review = utils_openreviewio.get_cache_media_review(
                                version_media_path
                            )
                            if not review:
                                review = orio.MediaReview(
                                    version_media_path, status=status
                                )

                            # Add text content
                            contents_list.append(
                                orio.Content.TextComment(body=comment.get("text"))
                            )
                            # Create Note, with parent if not new version
                            note = orio.Note(
                                author="stax",
                                date=str(
                                    datetime.fromisoformat(
                                        comment.get("updated_at")
                                    ).replace(tzinfo=tz)
                                ),
                                contents=contents_list,
                                parent=previous_note if not new_version else None,
                            )

                            # Keep note if new version
                            if new_version:
                                previous_note = note

                            # Add note to ORIO review
                            review.add_note(note)

                            review.metadata = {
                                "task_id": task.get("id"),
                                "task_status_id": task.get("task_status_id"),
                            }

                            # Write to Stax' cache
                            utils_openreviewio.write_review_to_cache(review)

                    #    Build timeline (not merged with upper loop for better readability)
                    # Associate to track
                    for t in timeline.tracks:
                        if t.name == task.get("task_type_name"):
                            task_track = t
                            break
                    else:
                        task_track = otio.schema.Track()
                        task_track.name = task.get("task_type_name")
                        task_track.metadata["type"] = "task"
                        timeline.tracks.append(task_track)

                    # Create the shot
                    shot_stack = otio.schema.Stack()
                    # Shot stack name is the sequence's name in Player Mode, has to be unique
                    shot_stack.name = f"{seq.get('name')}_{shot.get('name')}_{task.get('task_type_name')}"
                    shot_stack.source_range = otio.opentime.range_from_start_end_time(
                        start_time, end_time_exclusive
                    )

                    #   Shot versions
                    # Create a track for every version, the last version should be on the last track
                    # NB: In this demo, only the first shot has two versions
                    for i, ver in enumerate(versions_list, 1):
                        version_track = otio.schema.Track()
                        # Version track name is the sequence's name in Note Mode, has to be unique
                        version_track.name = ver.name

                        # Version clip
                        clip = otio.schema.Clip()
                        clip.media_reference = otio.schema.ExternalReference(str(ver))

                        version_track.append(clip)

                        # Is user allowed to review?
                        # You can set this value to False to lock the version in Stax, reviewing won't be allowed.
                        if task.get("task_type_name") == "Animation":
                            version_track.metadata["review_enabled"] = False

                        shot_stack.append(version_track)

                    task_track.append(shot_stack)

        # Write the timeline
        otio.adapters.write_to_file(timeline, timeline_path)

        return timeline_path

    def review_session_publish(self, edited_notes):
        # /?\ Using note.export and review.import_note you can store .zip files to import notes directly.
        # Please refer to https://pypi.org/project/openreviewio/

        for review, note in edited_notes:
            # Get kitsu elements
            task = gazu.task.get_task(review.metadata.get("task_id"))

            # Set note status for task
            if review.status.state == "rejected":
                note_status = "Retake"
            elif review.status.state == "approved":
                note_status = "Done"

            new_task_status = gazu.task.get_task_status_by_name(note_status)

            # Contents
            # Create comment from text
            comment = None
            if note:
                for content in note.contents:
                    if content.__class__ is orio.TextComment:  # Text
                        comment = gazu.task.add_comment(
                            task, new_task_status, content.body
                        )

            # Create default comment if no comment already created and note exists and has contents
            if not comment and note and note.contents:
                comment = gazu.task.add_comment(task, new_task_status)

            # Attach annotation images as previews
            if note and comment:
                for content in note.contents:
                    if content.__class__ is orio.ImageAnnotation:  # Image Annotation
                        gazu.task.add_preview(task, comment, content.reference_image)
                        gazu.task.add_preview(task, comment, content.path_to_image)

    def get_webpage_url(self, element_name):
        # For now it works only for the tasks related to shots, the following code is not generic at all
        task = gazu.task.get_task(element_name)

        element_path = [
            "productions",
            task.get("project_id"),
            "episodes",
            task.get("episode").get("id"),
            "shots",
            "tasks",
            task.get("id"),
        ]

        server_url = self.kitsu_host.split("/")[:-1]

        webpage_url = "/".join(server_url + element_path)

        return webpage_url

    # ======= Custom functions ========
    # You can create your functions here and access them `get_linker().my_function()`
    #   while you don't use a stax callback function name. See Callbacks_Script.py
    def get_projects(self):
        return ["Caminandes"]

    def get_tasks(self, project_name, timeline_name, user_name):
        project = gazu.project.get_project_by_name(project_name)
        episode = gazu.shot.get_episode_by_name(project, timeline_name)
        sequences = gazu.shot.all_sequences_for_episode(episode)

        shots = []
        for seq in sequences:
            shots.extend(gazu.shot.all_shots_for_sequence(seq))

        tracks = gazu.task.all_task_types_for_shot(shots[0])
        tracks = sorted(tracks, key=lambda k: k["priority"])  # Sorted by priority
        tracks = [task.get("name") for task in tracks]

        return tracks

    def set_parameters(self, project_name: str, timeline_name: str, tasks_list):
        """Set class attributes to be used as parameters at the timeline creation

        :param project_name: Name of the project
        :type project_name: str
        :param timeline_name: [description]
        :type timeline_name: [type]
        :param tasks_list: [description]
        :type tasks_list: [type]
        """
        # Modify class attributes because the instance changes during timeline loading
        self.__class__.demo_directory = Path(
            utils_cache.get_cache_directory()
        ).joinpath("Demo_Files")

        self.__class__.project_name = project_name

        self.__class__.tasks_list = [t.name for t in tasks_list]

        return self.demo_directory.joinpath(f"{timeline_name}.otio")

    def get_timelines_name_list(self, project_name: str = "") -> list:
        project = gazu.project.get_project_by_name(project_name)
        episodes = gazu.shot.all_episodes_for_project(project)
        return [e.get("name") for e in episodes]


@persistent
def update_user_tasks_list(self, context):
    """Update the list of available tasks for the user.
    :param self: Current operator running this function
    :param context: Blender context
    """
    scene = context.scene
    user_prefs = scene.user_preferences

    # Load tasks
    user_config = (
        utils_config.read_user_config()
    )  # TODO: User config loading has to be optimized

    # Set available timelines
    self.available_timelines.clear()
    for t in get_linker().get_timelines_name_list(self.project_name):
        tl = self.available_timelines.add()
        if type(t) is tuple and len(t) > 1:
            tl.name, tl.description = t
        else:
            tl.name = str(t)

    if not self.timeline_name:  # default timeline
        self.timeline_name = self.available_timelines[0].name

    # Create tracks in scene.tracks
    self.tasks_list.clear()

    tasks = get_linker().get_tasks(
        self.project_name, self.timeline_name, user_prefs.user_login
    )

    for task in tasks:
        new_task = self.tasks_list.add()
        new_task.name = task

        # Set synchronization state from user config
        if (
            user_config.get(self.project_name)
            and task in user_config.get(self.project_name).keys()
        ):
            setattr(new_task, "displayed", user_config.get(self.project_name).get(task))


def get_projects_as_items(self, context):
    """Give every project name as blender enum tuple item
    :return: Projects as tuples
    """
    projects = get_linker().get_projects()
    projects_as_items = []
    for project in projects:
        projects_as_items.append((project, project, project))

    return projects_as_items


# ==== Custom Property group ====
# Theses groups are necessary for the custom Timeline Loader below


class AvailableTimeline(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(name="Name")
    description: bpy.props.StringProperty(name="Description")


# ==== Custom Timeline Loader interface =====
# This allows you to customize your interface as you like
# And also to set the parameters you need to load from the command line
# In this example you can do it with:
# bpy.ops.sequencer.load_timeline(project_name="Caminandes", timeline_name="E01")
# instead of the basic one:
# bpy.ops.sequencer.load_timeline(filepath="/path/to/timeline.otio")


class LoadTimeline(bpy.types.Operator):
    """Load episode from the demo Kitsu server"""

    bl_idname = "sequencer.load_timeline"
    bl_label = "Load episode"

    project_name: bpy.props.EnumProperty(
        items=get_projects_as_items,
        name="Project",
        description="Project Name",
        update=update_user_tasks_list,
    )

    timeline_name: bpy.props.StringProperty(
        name="Timeline name",
        description="Choose timeline to load",
    )

    available_timelines: bpy.props.CollectionProperty(type=AvailableTimeline)
    tasks_list: bpy.props.CollectionProperty(type=Track)

    # This parameter is the only required one to target the timeline to load
    filepath: bpy.props.StringProperty()

    def invoke(self, context, event):
        # Update tasks to be selected
        update_user_tasks_list(self, context)

        # Load config data
        config_data = stax.utils.utils_config.read_user_config()

        self.project_name = config_data.get("project", "Caminandes")
        self.timeline_name = config_data.get("timeline", "E01")
        for task in self.tasks_list:
            task.displayed = config_data.get("tasks", {}).get(task.name, True)

        return context.window_manager.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout

        # Select timeline
        layout.prop(self, "project_name")
        layout.prop_search(
            self, "timeline_name", self, "available_timelines", text="Timeline"
        )

        # Display description if provided
        user_timeline = self.available_timelines.get(self.timeline_name)
        if hasattr(user_timeline, "description"):
            timeline_description = user_timeline.description
            if timeline_description:
                layout.label(text=timeline_description)

        # Draw project tasks into popup dialog
        for task in reversed(self.tasks_list):
            layout.prop(task, "displayed", text=task.name)

    def execute(self, context):
        # Waiting cursor
        context.window.cursor_set("WAIT")

        # Set parameters
        timeline_path = get_linker().set_parameters(
            self.project_name,
            self.timeline_name,
            self.tasks_list,
        )

        # Save parameters in user config
        stax.utils.utils_config.write_user_config(
            {
                "project": self.project_name,
                "timeline": self.timeline_name,
                "tasks": {t.name: t.displayed for t in self.tasks_list},
            }
        )

        # Original LoadTimeline operator in ops_timeline.py
        self.filepath = str(timeline_path)
        stax.ops.ops_timeline.LoadTimeline.execute(self, context)

        # Link Reviews
        bpy.ops.scene.link_reviews(
            directory=str(utils_cache.get_orio_default_cache_directory())
        )

        return {"FINISHED"}


# Do not try to store the classes to override in a set out of register(),
#  at this moment it won't work because stax' submodules aren't loaded.


def register():
    # Override timeline_load for custom behavior
    # Clear Default UI
    unregister_class(stax.ops.ops_timeline.LoadTimeline)

    # Set Custom UI
    register_class(AvailableTimeline)
    register_class(LoadTimeline)

    print(f"Registered Callback {__file__}")


def unregister():
    # Set back classes
    # Unregister Custom UI
    unregister_class(LoadTimeline)
    unregister_class(AvailableTimeline)

    # Register Default UI
    register_class(stax.ops.ops_timeline.LoadTimeline)

    print(f"Unregistered Callback {__file__}")
